package test_classes;

public final class Cargo implements Cloneable {
    private final Dimensions dimensions;
    private final double weight;
    private String deliveryAddress;
    private boolean canFlip;
    private String registrationNumber;
    private boolean fragile;

    public Cargo(Dimensions dimensions, double weight, String deliveryAddress, boolean canFlip, String registrationNumber, boolean fragile) {
        this.dimensions = dimensions;
        this.weight = weight;
        this.deliveryAddress = deliveryAddress;
        this.canFlip = canFlip;
        this.registrationNumber = registrationNumber;
        this.fragile = fragile;
    }

    // Метод для получения объема груза
    public double calculateVolume() {
        return dimensions.getWidth() * dimensions.getHeight() * dimensions.getLength();
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public Cargo setDeliveryAddress(String deliveryAddress) {
        Cargo cargo = (Cargo) clone();
        cargo.deliveryAddress = deliveryAddress;
        return cargo;
    }

    public boolean isCanFlip() {
        return canFlip;
    }

    public void setCanFlip(boolean canFlip) {
        this.canFlip = canFlip;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public boolean isFragile() {
        return fragile;
    }

    public void setFragile(boolean fragile) {
        this.fragile = fragile;
    }

    // Метод для создания копии объекта
    @Override
    public Cargo clone() {
        try {
            return (Cargo) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(); // В этом случае клонирование всегда поддерживается
        }
    }
}
