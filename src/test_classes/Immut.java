package test_classes;

public final class Immut implements Cloneable{
    private int value;

    public Immut(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
