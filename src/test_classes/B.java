package test_classes;

public final class B {
    private int b;
    public B(int newB) {
        b = newB;
    }

    public int getB() {
        return b;
    }

    public void setB(int newB) {
        b = newB;
    }

}
